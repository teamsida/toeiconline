// default package
// Generated Aug 14, 2016 3:24:19 PM by Hibernate Tools 4.0.1.Final
package org.webapp.toeiconline.entity;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Lanthi generated by hbm2java
 */
@Entity
@Table(name = "lanthi", catalog = "toeiconline")
public class Lanthi implements java.io.Serializable {

    private Integer maLanThi;
    private Nguoidung nguoidung;
    private Dethi dethi;
    private Date tgbatDau;
    private Date tgketThuc;
    private Boolean trangThai;
    private Set<Cautraloi> cautralois = new HashSet<Cautraloi>(0);

    public Lanthi() {
    }

    public Lanthi(Nguoidung nguoidung, Dethi dethi, Date tgbatDau,
            Date tgketThuc, Boolean trangThai, Set<Cautraloi> cautralois) {
        this.nguoidung = nguoidung;
        this.dethi = dethi;
        this.tgbatDau = tgbatDau;
        this.tgketThuc = tgketThuc;
        this.trangThai = trangThai;
        this.cautralois = cautralois;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "MaLanThi", unique = true, nullable = false)
    public Integer getMaLanThi() {
        return this.maLanThi;
    }

    public void setMaLanThi(Integer maLanThi) {
        this.maLanThi = maLanThi;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MaND")
    public Nguoidung getNguoidung() {
        return this.nguoidung;
    }

    public void setNguoidung(Nguoidung nguoidung) {
        this.nguoidung = nguoidung;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MaDe")
    public Dethi getDethi() {
        return this.dethi;
    }

    public void setDethi(Dethi dethi) {
        this.dethi = dethi;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TGBatDau", length = 19)
    public Date getTgbatDau() {
        return this.tgbatDau;
    }

    public void setTgbatDau(Date tgbatDau) {
        this.tgbatDau = tgbatDau;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TGKetThuc", length = 19)
    public Date getTgketThuc() {
        return this.tgketThuc;
    }

    public void setTgketThuc(Date tgketThuc) {
        this.tgketThuc = tgketThuc;
    }

    @Column(name = "TrangThai")
    public Boolean getTrangThai() {
        return this.trangThai;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lanthi")
    public Set<Cautraloi> getCautralois() {
        return this.cautralois;
    }

    public void setCautralois(Set<Cautraloi> cautralois) {
        this.cautralois = cautralois;
    }

}

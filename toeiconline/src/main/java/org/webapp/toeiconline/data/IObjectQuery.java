package org.webapp.toeiconline.data;


import java.util.List;

public interface IObjectQuery<in,out> {
   public out query(in message);
}

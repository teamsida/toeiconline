package org.webapp.toeiconline.data.handler;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.webapp.toeiconline.data.NguoidungDAO;
import org.webapp.toeiconline.entity.Nguoidung;
/*@Transactional*/
public class NguoidungDAOImpl implements NguoidungDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    public List<Nguoidung> getNguoidungs() {
        Session session = this.sessionFactory.getCurrentSession();

        @SuppressWarnings("unchecked")
        List<Nguoidung> list = session.createQuery("from Nguoidung").list();

        return list;
      
    }

}

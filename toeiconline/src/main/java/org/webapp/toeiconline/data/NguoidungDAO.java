package org.webapp.toeiconline.data;
import java.util.List;

import org.webapp.toeiconline.entity.Nguoidung;


public interface NguoidungDAO { 
  public  List<org.webapp.toeiconline.entity.Nguoidung> getNguoidungs();
}

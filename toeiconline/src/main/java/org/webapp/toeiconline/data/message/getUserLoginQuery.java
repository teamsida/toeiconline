package org.webapp.toeiconline.data.message;

public class getUserLoginQuery {
    private String userName;
    private String passWord;
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassWord() {
        return passWord;
    }
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
    public getUserLoginQuery(String _userName, String _passWord){
        this.passWord=_passWord;
        this.userName=_userName;
    }
}

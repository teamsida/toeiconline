package org.webapp.toeiconline.data.handler;
import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.hibernate.transform.ResultTransformer;
import org.springframework.transaction.annotation.Transactional;
import org.webapp.toeiconline.data.IObjectQuery;
import org.webapp.toeiconline.data.NguoidungDAO;
import org.webapp.toeiconline.data.message.getUserLoginQuery;
import org.webapp.toeiconline.pojo.NguoidungLoginPojo;


@Transactional
public class NguoidungQueryHandler implements
        IObjectQuery<getUserLoginQuery, List<NguoidungLoginPojo>> {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    NguoidungDAO nguoidungDao;

    public List<NguoidungLoginPojo> query(getUserLoginQuery message) {
        Session session = this.sessionFactory.getCurrentSession();
        String sqlQString = "select new org.webapp.toeiconline.pojo.NguoidungLoginPojo( u.username, u.hoTen,u.email)"
                + " from Nguoidung as u where u.username= :username and u.password = :password";
        Query query = session.createQuery(sqlQString);
        query.setParameter("username", message.getUserName());
        query.setParameter("password", message.getPassWord());

        List<NguoidungLoginPojo> lstNguoidung = query.list();
        return lstNguoidung.size() == 0 ? null : lstNguoidung;
    }

    
}
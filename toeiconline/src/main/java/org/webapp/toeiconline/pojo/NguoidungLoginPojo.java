package org.webapp.toeiconline.pojo;

public class NguoidungLoginPojo {
    private String hoTen ;
    private String userName;
    private String email;
    public String getHoTen() {
        return hoTen;
    }
    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public NguoidungLoginPojo(String _hoTen, String _userName, String _email){
        this.hoTen= _hoTen;
        this.userName=_userName;
        this.email=_email;
    }
}

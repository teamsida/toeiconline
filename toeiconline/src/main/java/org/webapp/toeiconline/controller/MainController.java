package org.webapp.toeiconline.controller;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.webapp.toeiconline.data.IObjectQuery;
import org.webapp.toeiconline.data.message.getUserLoginQuery;
import org.webapp.toeiconline.entity.Nguoidung;
import org.webapp.toeiconline.pojo.NguoidungLoginPojo;


@Controller
public class MainController {
    @Autowired
    private IObjectQuery dataQuery;
   /* private NguoidungDAO nguoidungDAO;*/
    @RequestMapping({ "/index"})
    public String index(Model model) {
        return "index";
    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute(value = "acc") Nguoidung acc, Model model,
            HttpSession session) {

        List<NguoidungLoginPojo> lstnguoidungs = (List<NguoidungLoginPojo>) dataQuery
        .query(
                   new getUserLoginQuery(acc.getUsername()
                           ,acc.getPassword())
                );
        NguoidungLoginPojo nguoidungLoginPojo =lstnguoidungs.get(0);
        if (nguoidungLoginPojo == null) {
            model.addAttribute("message", "Error: invalid account!");
            return "login";
        } else {
            
            session.setAttribute("username", nguoidungLoginPojo.getUserName());
            
            session.setAttribute("hoten", nguoidungLoginPojo.getHoTen());
            
            return "redirect:/index.html";
        }
    }
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("acc", new Nguoidung());
        return "login";
    }
    @RequestMapping(value = "/about")
    public String about() {
        return "about";
    }

}
<%-- 
    Document   : login
    Created on : Jun 1, 2015, 1:27:10 AM
    Author     : DG
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="me" uri="/WEB-INF/tlds/mytags.tld" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<me:template title="Login">
    <jsp:attribute name="content">
        
        <script type="text/javascript">
            function validateRes() {
                if(document.frmRes.username.value == "") {
                  alert("Please enter your user name.");
                  document.frmRes.username.focus();
                  return false;
                } else if(document.frmRes.password.value == "") {
                  alert("Please enter your password.");
                  document.frmRes.password.focus();
                  return false;
                } else if(document.frmRes.password.value != document.frmRes.password2.value) {
                  alert("Password 2 incorrect.");
                  document.frmRes.password2.focus();
                  return false;
                } else if(document.frmRes.hoTen.value == "") {
                  alert("Please enter your name.");
                  document.frmRes.hoTen.focus();
                  return false;
                } else if(document.frmRes.email.value == "") {
                  alert("Please enter your email.");
                  document.frmRes.email.focus();
                  return false;
                }
                var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                if (re.test(document.frmRes.email.value) == false) {
                    alert("Email invalid!");
                    document.frmRes.email.focus();
                    return false;
                }
                return true;
            }
            
            function validateLogin() {
                if(document.frmLogin.username.value == "") {
                  alert("Please enter your user name.");
                  document.frmLogin.username.focus();
                  return false;
                } else if(document.frmLogin.password.value == "") {
                  alert("Please enter your password.");
                  document.frmLogin.password.focus();
                  return false;
                }
                return true;
            }
        </script>
        
        <div class="login">
          <div class="wrap">
            <div class="col_1_of_login span_1_of_login">
                <h4 class="title">New User</h4>
                <p>By creating an account with our website, you will be able to practice toeic faster, store multiple toeic quiz, view and track new quiz in our website and more.</p>
                <div class="comments-area">
                    <form:form name="frmRes" method="POST" commandName="acc" onsubmit="return validateRes()" action="${pageContext.request.contextPath}/account/register.html">
                        <p>
                            <label>User Name</label>
                            <span>*</span>
                            <span style="color: red;">${errResUserName}</span>
                            <form:input path="username"/>
                        </p>
                        <p>
                            <label>Password</label>
                            <span>*</span>
                            <form:password path="password" />
                        </p>
                        <p>
                            <label>Verify Password</label>
                            <span>*</span>
                            <input type="password" name="password2" />
                        </p>
                         <p>
                            <label>Full Name</label>
                            <span>*</span>
                            <form:input path="hoTen"/>
                        </p>
                        <p>
                            <label>Email</label>
                            <span>*</span>
                            <span style="color: red;">${errResEmail}</span>
                            <form:input path="email"/>
                        </p>
                        <p>By clicking 'Create Account' you agree to the Terms &amp; Conditions.</p>
                         <p>
                             <input type="submit" value="Create An Account">
                        </p>
                    </form:form>
               </div>
            <div class="clear"></div>
            </div>
            <div class="col_1_of_login span_1_of_login">
              <div class="login-title">
            <h4 class="title">Registered User</h4>
                <div class="comments-area">
                    <form:form name="frmLogin" method="POST" onsubmit="return validateLogin()" modelAttribute="acc" action="${pageContext.request.contextPath}/login.html">
                        <p>
                            <label>Name</label>
                            <span>*</span>
                            <form:input path="username"/>
                        </p>
                        <p>
                            <label>Password</label>
                            <span>*</span>
                            <form:password path="password" />
                            <span style="color: red;">${message}</span>
                        </p>
                         <p id="login-form-remember">
                             Forget Your Password ?
                         </p>
                         <p>
                            <input type="submit" value="Login">
                        </p>
                    </form:form>
               </div>
          </div>
            </div>
            <div class="clear"></div>
            </div>
        </div>
        
    </jsp:attribute>
</me:template>

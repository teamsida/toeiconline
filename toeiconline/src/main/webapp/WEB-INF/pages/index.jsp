<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="me" uri="/WEB-INF/tlds/mytags.tld" %>

<me:template title="Index">
    <jsp:attribute name="content">
        
        <!-- start slider -->
      <div class="wmuSlider example1" style="height: 560px;">
    <div class="wmuSliderWrapper">
          <article style="position: relative; width: 100%; opacity: 1;">
        <div class="banner-wrap">
              <div class="slider-left"> <img src="${pageContext.request.contextPath}/resources/template/images/banner (2).jpg" alt=""/> </div>
              <div class="clear"></div>
            </div>
      </article>
          <article style="position: absolute; width: 100%; opacity: 0;">
        <div class="banner-wrap">
              <div class="slider-left"> <img src="${pageContext.request.contextPath}/resources/template/images/banner (3).jpg" alt=""/> </div>
              <div class="clear"></div>
            </div>
      </article>
        </div>
    <a class="wmuSliderPrev">Previous</a><a class="wmuSliderNext">Next</a>
    <ul class="wmuSliderPagination">
          <li><a href="#" class="">0</a></li>
          <li><a href="#" class="">1</a></li>
        </ul>
    <a class="wmuSliderPrev">Previous</a> <a class="wmuSliderNext">Next</a>
    <ul class="wmuSliderPagination">
          <li><a href="#" class="wmuActive">0</a></li>
          <li><a href="#" class="">1</a></li>
        </ul>
  </div>
      <script src="${pageContext.request.contextPath}/resources/template/js/jquery.wmuSlider.js"></script> 
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/template/js/modernizr.custom.min.js"></script> 
      <script>
                             $('.example1').wmuSlider();         
                        </script> 

<!-- end slider --> 

<!-- start main content -->
<div class="main">
      <div class="wrap">
    <div class="content-bottom">
        
        <!-- start content -->
        <div class="content-bottom">
          <div class="box1">
        <div class="col_1_of_3 span_1_of_3"><a href="${pageContext.request.contextPath}/courses/quizzes.html">
          <div class="view view-fifth">
            <div class="top_box">
              <h3 class="m_1">Toeic Test</h3>
              <p class="m_2">New</p>
              <div class="grid_img">
                <div class="css3"><img src="${pageContext.request.contextPath}/resources/template/images/pic.jpg" alt=""/></div>
                <div class="mask">
                  <div class="info">Quick View</div>
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          </a></div>
        <div class="col_1_of_3 span_1_of_3"><a href="${pageContext.request.contextPath}/courses/quizzes.html">
          <div class="view view-fifth">
            <div class="top_box">
              <h3 class="m_1">Toeic Test</h3>
              <p class="m_2">New</p>
              <div class="grid_img">
                <div class="css3"><img src="${pageContext.request.contextPath}/resources/template/images/pic.jpg" alt=""/></div>
                <div class="mask">
                  <div class="info">Quick View</div>
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          </a></div>
        <div class="col_1_of_3 span_1_of_3"><a href="${pageContext.request.contextPath}/courses/quizzes.html">
          <div class="view view-fifth">
            <div class="top_box">
              <h3 class="m_1">Toeic Test</h3>
              <p class="m_2">New</p>
              <div class="grid_img">
                <div class="css3"><img src="${pageContext.request.contextPath}/resources/template/images/pic.jpg" alt=""/></div>
                <div class="mask">
                  <div class="info">Quick View</div>
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          </a></div>
      </div>
          <div class="box1">
        <div class="col_1_of_3 span_1_of_3"><a href="${pageContext.request.contextPath}/courses/quizzes.html">
          <div class="view view-fifth">
            <div class="top_box">
              <h3 class="m_1">Toeic Test</h3>
              <p class="m_2">New</p>
              <div class="grid_img">
                <div class="css3"><img src="${pageContext.request.contextPath}/resources/template/images/pic.jpg" alt=""/></div>
                <div class="mask">
                  <div class="info">Quick View</div>
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          </a></div>
        <div class="col_1_of_3 span_1_of_3"><a href="${pageContext.request.contextPath}/courses/quizzes.html">
          <div class="view view-fifth">
            <div class="top_box">
              <h3 class="m_1">Toeic Test</h3>
              <p class="m_2">New</p>
              <div class="grid_img">
                <div class="css3"><img src="${pageContext.request.contextPath}/resources/template/images/pic.jpg" alt=""/></div>
                <div class="mask">
                  <div class="info">Quick View</div>
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          </a></div>
        <div class="col_1_of_3 span_1_of_3"><a href="${pageContext.request.contextPath}/courses/quizzes.html">
          <div class="view view-fifth">
            <div class="top_box">
              <h3 class="m_1">Toeic Test</h3>
              <p class="m_2">New</p>
              <div class="grid_img">
                <div class="css3"><img src="${pageContext.request.contextPath}/resources/template/images/pic.jpg" alt=""/></div>
                <div class="mask">
                  <div class="info">Quick View</div>
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          </a></div>
      </div>
          <div class="clear"></div>
        </div>
        
        <!-- end content -->
        
          <div class="clear"></div>
        </div>
  </div>
    </div>
<!-- end main content --> 
        
    </jsp:attribute>
</me:template>
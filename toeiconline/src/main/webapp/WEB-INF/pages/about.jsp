<%-- 
    Document   : about
    Created on : May 31, 2015, 11:48:03 PM
    Author     : DG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="me" uri="/WEB-INF/tlds/mytags.tld" %>

<me:template title="About">
    <jsp:attribute name="content">
        
    <div class="wrap">
        <div class="content-bottom">
            <div class="view view-fifth">
                <h3 class="m_1">About</h3>
            </div>
            <div class="clear"></div>
        </div>
    </div>
        
    </jsp:attribute>
</me:template>
<%-- 
    Document   : template
    Created on : May 31, 2015, 11:55:22 PM
    Author     : DG
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="title"%>
<%@attribute name="content" fragment="true" %>

<!DOCTYPE HTML>
<html>
	<head>
	<title>${title}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="${pageContext.request.contextPath}/resources/template/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/template/js/jquery-1.11.3.js"></script>
	<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
	<!-- start menu -->
	<link href="${pageContext.request.contextPath}/resources/template/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/template/js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
	<!-- end menu -->
	<!-- top scrolling -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/template/js/move-top.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/template/js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
	</head>
<body>
<!-- start header top -->
<div class="header-top">
    <div class="wrap">
        <div class="logo"> 
            <a href="${pageContext.request.contextPath}/index.html">
                <img src="${pageContext.request.contextPath}/resources/template/images/logo.jpg" alt=""/>
            </a> 
        </div>
        
        <div class="cssmenu">
            <c:choose>
                <c:when test="${sessionScope.username != null}">
                    <ul>
                        <li class="active"><a href="${pageContext.request.contextPath}/index.html">${sessionScope.hoten}</a></li>|
                        <li><a href="${pageContext.request.contextPath}/account/logout.html">Log Out</a></li>
                    </ul>
                </c:when>
                <c:otherwise>
                    <ul>
                        <li class="active"><a href="${pageContext.request.contextPath}/login.html">Sign up | Log In</a></li>
                    </ul>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!-- end header top --> 

<!-- start header bottom -->
<div class="header-bottom">
    <div class="wrap"> 
    <!-- start header menu -->
      <ul class="megamenu skyblue">
        <li><a class="color1" href="${pageContext.request.contextPath}/index.html">Home</a></li>
        <li class="grid"><a class="color2" href="#">Toeic Test</a>
        <div class="megapanel">
            <div class="row">
            <div class="col1">
                  <div class="h_nav">
                <h4>New</h4>
                <ul>
                      <li><a href="${pageContext.request.contextPath}/courses/quizzes.html">Test 1</a></li>
                      <li><a href="${pageContext.request.contextPath}/courses/quizzes.html">Test 2</a></li>
                      <li><a href="${pageContext.request.contextPath}/courses/quizzes.html">Test 3</a></li>
                    </ul>
              </div>
                  
            </div>
            
          </div>
            </div>
        </li>
          <li><a class="color11" href="${pageContext.request.contextPath}/about.html">About Us</a></li>
          
          <c:if test="${sessionScope.permission != null}">
            <li><a class="color12" href="${pageContext.request.contextPath}/admin.html">Management</a></li>
          </c:if>
      </ul>
    <!-- end header menu -->
    <div class="clear"></div>
  </div>
    </div>
<!-- end header bottom --> 

<!-- start main -->
<div class="main">
    <jsp:invoke fragment="content"/>
</div>
<!-- end main -->

<!-- start footer -->
<div class="footer">
      <div class="footer-top">
    <div class="wrap">
          <p>TOEIC is a registered trademark of the Educational Testing Service (ETS). These online courses are not endorsed or approved by ETS.</p>
          <div class="clear"></div>
        </div>
  </div>
      <div class="copy">
    <div class="wrap">
          <p>Copyright By Team 14 | © All rights reserved</p>
        </div>
  </div>
    </div>
<!-- end footer --> 

<script type="text/javascript">
    $(document).ready(function() {

            var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear' 
            };


            $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script> 

<a href="#" id="toTop" style="display: block;">
    <span id="toTopHover" style="opacity: 1;"></span>
</a>
</body>
</html>
